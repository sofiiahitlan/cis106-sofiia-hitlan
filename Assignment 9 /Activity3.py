def getExpressions():
    print("Enter number of expressions")
    expressionsNumber = int(input())
    
    return expressionsNumber

def getNumber():
    print("Enter numeric value you would like to see a list of multiplication expressions for")
    number = int(input())
    
    return number

def whileLoop():
    condition = 0
    expressionsNumber = getExpressions()
    number = getNumber()
    while condition < expressionsNumber:
        condition = condition + 1
        print(str(number) + " * " + str(condition) + " = " + str(number * condition))

# Main
# This program generates a list of multiplication expressions for a value entered by the user. The program will be running until the user wants to continue or until the user wants to stop.
while True:    #This simulates a Do Loop
    whileLoop()
    print("Would you like to try again? Enter y or n")
    loopResponse = input()
    if not(loopResponse == "y"): break   #Exit loop