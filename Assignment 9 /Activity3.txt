Module main()
    Declare String loopResponse
    
    // This program generates a list of multiplication expressions for a value entered by the user. The program will be running until the user wants to continue or until the user wants to stop.
    Do
        Call whileLoop()
        Display "Would you like to try again? Enter y or n"
        Input loopResponse
    While loopResponse == "y"
End Module

Function Integer getExpressions ()
    Declare Integer expressionsNumber
    
    Display "Enter number of expressions"
    Input expressionsNumber
    
    Return expressionsNumber
End Function

Function Integer getNumber ()
    Declare Integer number
    
    Display "Enter numeric value you would like to see a list of multiplication expressions for"
    Input number
    
    Return number
End Function

Module whileLoop ()
    Declare Integer condition
    Declare Integer expressionsNumber
    Declare Integer number
    
    Set condition = 0
    Set expressionsNumber = getExpressions()
    Set number = getNumber()
    While condition < expressionsNumber
        Set condition = condition + 1
        Display number, " * ", condition, " = ", number * condition
    End While
End Module