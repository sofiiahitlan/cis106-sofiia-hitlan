# This programs asks user to enter as many grades as he or she likes, and calculated the average. Values are accepted until the user enters no value.


def calculate_average(total, count):
    if count > 0:
        average = float(total) / float(count)
    else:
        average = 0

    return average


def display_result(average):
    print("Your score average is " + str(average))


def get_score():
    print("Enter your score (press Enter to stop):")
    score = input()    
    return score


def main():
    count = 0
    total = 0
    while True:    #This simulates a Do Loop
        score = get_score()
        if score == "": 
            break
        else:
            total = total + float(score)
            count = count + 1

    average = calculate_average(total, count)
    display_result(average)


main()
