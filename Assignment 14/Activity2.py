import os
    

def get_array():
    path = os.getcwd()
    scores = os.path.join(path, "scores.txt")
    file = open(scores, "r")
    array = []
    for line in file:
        comma = line.find(',')
        string_length = len(line)
        line = line.replace('\n', '')
        score = line[comma + 1:string_length]
        array.append(score)
    file.close()
    
    return array
 

def get_total(array):
    total = 0
    for index in range (1, len(array)):
        total = total + int(array[index])
    return total


def get_average(total, array):
    average = total / (len(array) - 1)
    average = round(average, 2)

    return average
 
        
def get_high_score(array):
    high = array[1]
    for index in range(2, len(array)):
        if high < array[index]:
            high = array[index]
            
    return high
    

def get_low_score(array):
    low = array[1]
    for index in range (2, len(array)):
        if low > array[index]:
            low = array[index]
            
    return low
  
    
def display_average(average):
    print("The average of entered scores is: " + str(average))
   
    
def display_high(high):
    print("The highest of entered scores is: " + high)
   
    
def display_low(low):
    print("The lowest of entered scores is: " + low)
    
    
def main():
    try:
        array = get_array()
    except:
        print("Error reading file")
        
    try:
        total = get_total(array)
    except:
        print("Missing data or wrong data type")
        
    try:
        average = get_average(total, array)
        high = get_high_score(array)
        low = get_low_score(array)
        display_average(average)
        display_high(high)
        display_low(low)
    except:
        print("Missing data")
        
main()
