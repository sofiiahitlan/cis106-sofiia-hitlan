# This program defines person's age in months, days and seconds


def get_age():
    print("Enter your age:")
    age = int(input())
    return age


def calculate_months(age):
    months = 12 * age
    return months


def calculate_days(age):
    days = 365 * age
    return days


def calculate_hours(age):
    hours = 8760 * age
    return hours


def calculate_seconds(age):
    seconds = 31557600 * age
    return seconds


def display_months(age, months):
    print(
        str(age) + " years old person is approximately " + str(months) + " months old"
    )


def display_days(age, days):
    print(str(age) + " years old person is approximately " + str(days) + " days old")


def display_hours(age, hours):
    print(str(age) + " years old person is approximately " + str(hours) + " hours old")


def display_seconds(age, seconds):
    print(
        str(age) + " years old person is approximately " + str(seconds) + " seconds old"
    )


def main():
    age = get_age()
    months = calculate_months(age)
    days = calculate_days(age)
    hours = calculate_hours(age)
    seconds = calculate_seconds(age)
    display_months(age, months)
    display_days(age, days)
    display_hours(age, hours)
    display_seconds(age, seconds)


main()
