def calculateannualy(hours, rate):
    annualy = hours * rate * 12
    
    return annualy

def calculatemonthly(hours, rate):
    monthly = hours * rate * 4
    
    return monthly

def calculateweekly(hours, rate):
    weekly = hours * rate
    
    return weekly

def displayresult(weekly, monthly, annualy):
    print("Your weekly gross pay is " + str(weekly))
    print("Your monthly gross pay is " + str(monthly))
    print("Your annual gross pay is " + str(annualy))

def gethours():
    print("How many hours per week do you work?")
    hours = int(input())
    
    return hours

def getrate():
    print("How much are you paid per hour?")
    rate = int(input())
    
    return rate

# Main
# This program calculates and displays weekly, monthly and annual gross pay
hours = gethours()
rate = getrate()
weekly = calculateweekly(hours, rate)
monthly = calculatemonthly(hours, rate)
annualy = calculateannualy(hours, rate)
displayresult(weekly, monthly, annualy)
