def getExpressions():
    print("Enter number of expressions")
    expressionsNumber = int(input())
    
    return expressionsNumber

def getNumber():
    print("Enter numeric value you would like to see a list of multiplication expressions for")
    number = int(input())
    
    return number

# Main
# This program uses for loop to generate a list of multiplication expressions for a given value. User enters the value and the number of expressions to be displayed.
condition = 1
number = getNumber()
expressionsNumber = getExpressions()
for multiply in range(condition, expressionsNumber + 1, 1):
    condition = condition + 1
    print(str(number) + " * " + str(multiply) + " = " + str(number * condition))