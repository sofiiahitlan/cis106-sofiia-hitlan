#This program can define person's age in months, days and seconds

print("Enter your age:")
age = int(input())

MONTHS_IN_A_YEAR = 12
DAYS_IN_A_YEAR = 365
HOURS_IN_A_YEAR = 8760
SECONDS_IN_A_YEAR = 31557600

months_in_the_age = age * MONTHS_IN_A_YEAR
days_in_the_age = age * DAYS_IN_A_YEAR
hours_in_the_age = age * HOURS_IN_A_YEAR
seconds_in_the_age = age * SECONDS_IN_A_YEAR

print(str(age) + " years old person is approximately " + str(months_in_the_age) + " months old")
print(str(age) + " years old person is approximately " + str(days_in_the_age) + " days old")
print(str(age) + " years old person is approximately " + str(hours_in_the_age) + " hours old")
print(str(age) + " years old person is approximately " + str(seconds_in_the_age) + " seconds old")

