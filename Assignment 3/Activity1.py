# This program calculates and displays weekly, montly and annual gross pay
# Variables are declared first
# Users are asked to input their data

print("How many hours per week do you work?")
hours = int(input())
print("What is your hourly rate?")
rate = int(input())

# Program does calculations
weeklypay = hours * rate
monthlypay = weeklypay * 4
annualpay = weeklypay * 52

# Weekly, monthly and annual pay are displayed
print("Your weekly gross pay is:")
print(weeklypay)
print("Your monthly gross pay is:")
print(monthlypay)
print("Your annual gross pay is:")
print(annualpay)
