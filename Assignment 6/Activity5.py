# This program calculates the area of a room to determine the amount of floor covering needed.


def get_length():
    print("Enter a length of your room in feet")
    length = float(input())
    return length


def get_width():
    print("Enter a width of your room in feet")
    width = float(input())
    return width


def calculate_feet(length, width):
    feet = length * width
    return feet


def calculate_yards(feet):
    yards = feet / 9
    return yards


def display_yards(yards):
    print(
        "You will need " + str(yards) + " square yards of floor covering for your room"
    )


def main():
    length = get_length()
    width = get_width()
    feet = calculate_feet(length, width)
    yards = calculate_yards(feet)
    display_yards(yards)


main()