# This program converts miles to kilometers, meters, and centimeters.


def get_miles():
    print(
        "Please provide mile value you would like to convert to kilometers, meters, and centimeters"
    )
    miles = int(input())
    return miles


def calculate_kilometers(miles):
    kilometers = 1.609344 * miles
    return kilometers


def calculate_meters(miles):
    meters = 1609.344 * miles
    return meters


def calculate_centimeters(miles):
    centimeters = 160934 * miles
    return centimeters


def display_kilometers(miles, kilometers):
    print(str(miles) + " miles are equal to " + str(kilometers) + " kilometers")


def display_m(miles, meters):
    print(str(miles) + " miles are equal to " + str(meters) + " meters")


def display_centimeters(miles, centimeters):
    print(str(miles) + " miles are equal to " + str(centimeters) + " centimeters")


def main():
    miles = get_miles()
    kilometers = calculate_kilometers(miles)
    meters = calculate_meters(miles)
    centimeters = calculate_centimeters(miles)
    display_kilometers(miles, kilometers)
    display_m(miles, meters)
    display_centimeters(miles, centimeters)


main()
