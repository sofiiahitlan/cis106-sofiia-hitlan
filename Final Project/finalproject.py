## This programs uses menu data from simple.xml file to create 4 arrays with names, descriptions, calories and prices of all menu items. Menu is displayed. Also, program calculates and displays total number of items on the menu, their average price and average calories per item.

import os

def get_array(title):
    try:   
        path = os.getcwd()
        menu = os.path.join(path, "simple.xml")
        file = open(menu, "r")
        array = []
        close_open_tag = len(title) + 1
        for line in file:
            tag_open = line.find(title)
            tag_close = line.find('</')
            if (tag_open != -1 and tag_close != -1):
                item = line[tag_open + close_open_tag:tag_close]
                array.append(item)
        file.close()
        
        return array
    except:
        print("Error reading", menu + ". File is missing")


def print_menu(name_array, price_array, description_array, calories_array):
    try:
        print("Menu:")
        for index in range (0, len(name_array)):
            print(name_array[index] + " - " + description_array[index] + " - " + calories_array[index] + " calories - " + "$" + price_array[index])
    except:
        print("Data is missing.")


def get_items_total(name_array):
    try:
        items_total = 0
        while items_total in range (0, len(name_array)):
            items_total = items_total + 1
        
        return items_total
    except:
        print("Data is missing.")


def get_calories_total(calories_array):
    try:
        calories_total = 0
        for index in range (0, len(calories_array)):
            calories_total = calories_total + int(calories_array[index])
        
        return calories_total
    except:
        print("Data is missing.")


def get_calories_average(calories_total, items_total):
    try:
        calories_average = calories_total / items_total
    
        return calories_average
    except:
        print("Data is missing.")


def get_total_price(price_array):
    try:
        total_price = 0
        for index in range (0, len(price_array)):
            total_price = total_price + float(price_array[index].replace("$", ""))
        total_price = round(total_price, 5)
        
        return total_price
    except:
        print("Data is missing.")   


def get_average_price(total_price, items_total):
    try:
        average_price = total_price / items_total
        average_price = round(average_price, 2)
        
        return average_price
    except:
        print("Data is missing.")


def print_results(items_total, calories_average, average_price):
    print("The total number of items on the menu, the average number of calories per item, and the average price per item are: \n" + str(items_total) + " items - " + str(calories_average) + " calories - $" + str(average_price))
  

def main():
    name_array = get_array("name")
    calories_array = get_array("calories")
    description_array = get_array("description")
    price_array = get_array("price")
    print_menu(name_array, price_array, description_array, calories_array)
    items_total = get_items_total(name_array)
    calories_total = get_calories_total(calories_array)
    calories_average = get_calories_average(calories_total, items_total)
    total_price = get_total_price(price_array)
    average_price = get_average_price(total_price, items_total)
    print_results(items_total, calories_average, average_price)
    
main()
