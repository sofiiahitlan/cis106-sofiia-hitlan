#This program asks user to enter his or her date of birth(day, month, year) and displays the name of the day user was born on.


def get_day():
    print("Please enter day you were born on: ")
    day = int(input())
    
    return day

def get_month():
    print("Please enter the name of month you were born in: ")
    month = str(input())
    
    return month
    
def get_year():
    print("Please enter the year you were born in (use four digits): ")
    year = int(input())
  
    return year
 

def  month_array():
    array_month_name = ["March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "January", "February"]
    array_month_number = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
    month = get_month()
    for index in range(0, 11):
        if month == array_month_name[index]:
            month = array_month_number[index]
            break

    return month


def calculate_weekday(day, month, year):
    weekday = int((day + (13 * (month + 1) / 5) + year % 100 + year % 100 / 4 + year / 400 + year / 20) % 7)
    
    return weekday


def convert_weekday(weekday):
    weekday_georgian = ["Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
    for index in range(0, 6):
        weekday = weekday_georgian[index]
        
    return weekday


def display_weekday(weekday):
    print("You were born on " + str(weekday) + ".")


def main():
    day = get_day()
    month = month_array()
    year = get_year()
    weekday = calculate_weekday(day, month, year)
    weekday = convert_weekday(weekday)
    display_weekday(weekday)
          
          
main()