# This program calculates weekly gross pay. All hours over 40 are calculated as overtime.


def calculateovertime(hours, rate):
    overtime = (40 + (hours - 40) * 1.5) * rate
    return overtime


def calculateregular(hours, rate):
    regular = hours * rate
    return regular


def displaypay(pay):
    print("Your gross weekly pay is " + str(pay))


def gethours():
    print("How many hours per week do you work?")
    hours = int(input())
    return hours


def getrate():
    print("How much are you paid per hour?")
    rate = int(input())
    return rate


def main():
    hours = gethours()
    rate = getrate()
    if hours > 40:
        pay = calculateovertime(hours, rate)
    else:
        pay = calculateregular(hours, rate)
    displaypay(pay)


main()
