def calculatecentimeters(miles):
    centimeters = 160934.4 * miles
    displaycentimeters(miles, centimeters)

    return centimeters


def calculatefeet(miles):
    feet = 5280 * miles
    displayfeet(miles, feet)

    return feet


def calculateinches(miles):
    inches = miles * 63360
    displayinches(miles, inches)

    return inches


def calculatekilometers(miles):
    kilometers = 1.609344 * miles
    displaykilometers(miles, kilometers)

    return kilometers


def calculatemeters(miles):
    meters = 1609.344 * miles
    displaymeters(miles, meters)

    return meters


def calculateyards(miles):
    yards = miles * 1760
    displayyards(miles, yards)

    return yards


def displaycentimeters(miles, centimeters):
    print(str(miles) + " miles are equal to " + str(centimeters) + " centimeters")


def displayfeet(miles, feet):
    print(str(miles) + " miles are equal to " + str(feet) + " feet")


def displayinches(miles, inches):
    print(str(miles) + " miles are equal to " + str(inches) + " inches")


def displaykilometers(miles, kilometers):
    print(str(miles) + " miles are equal to " + str(kilometers) + " kilometers")


def displaymeters(miles, meters):
    print(str(miles) + " miles are equal to " + str(meters) + " meters")


def displayyards(miles, yards):
    print(str(miles) + " miles are equal to " + str(yards) + " yards")


def getchoice():
    print(
        "Would you like to view miles in US measurements (yards, feet, and inches) or in metric measurements (kilometers, meters, and centimeters)?"
    )
    choice = input()

    return choice


def getmiles():
    print("Please enter the distance in miles")
    miles = int(input())

    return miles


# Main
# This program asks user to input the distance in miles. If user interested in converting it to metric system, program will display the result in kilometers, meters and centimeters.
miles = getmiles()
choice = getchoice()
if (
    choice == "us"
    or choice == "US"
    or choice == "US measurements"
    or choice == "us measurements"
):
    calculateyards(miles)
    calculatefeet(miles)
    calculateinches(miles)
else:
    if (
        choice == "metric"
        or choice == "Metric"
        or choice == "metric measurements"
        or choice == "Metric measurements"
    ):
        calculatekilometers(miles)
        calculatemeters(miles)
        calculatecentimeters(miles)
    else:
        print("Error. Please select between metric or US measurements")