#This program displays an array of enterered scores in the order from the highest to the lowest one. Loop is used to get an array of scores. User has to enter number of scores to start.

#Reference: https://en.wikibooks.org/wiki/Python_Programming/Lists#List_creation


def get_score():
    print("Please enter a score")
    score = int(input())
    
    return score


def get_scores_number():
    print("How many scores would you like to enter?")
    scores_number = int(input())
    
    return scores_number


def get_array(scores_number):
    scores_array = []
    condition = 0
    while condition < scores_number:
        score = get_score()
        scores_array.append(score)
        condition = condition + 1
    
    return scores_array


def sort_scores(scores_array):
    scores_array.sort(reverse = True)


def display_reverse_sort(scores_array):
    print("Here are the scores you've entered in order from the highest to the lowest one: ")
    for index in range(len(scores_array)):
        print(scores_array[index])
    

def main():
    scores_number = get_scores_number()
    scores_array = get_array(scores_number)
    sort_scores(scores_array)
    display_reverse_sort(scores_array)
  
  
main()