#This program calculates and displays the high, low, and average for the entered scores. Loop is used to create an array of scores. User has to enter number of scores to start.

#Reference: https://books.trinket.io/pfe/08-lists.html#lists-and-functions


def get_score():
    print("Please enter a score")
    score = int(input())
    
    return score


def get_scores_number():
    print("How many scores would you like to enter?")
    scores_number = int(input())
    
    return scores_number


def get_array(scores_number):
    scores_array = []
    condition = 0
    while condition < scores_number:
        score = get_score()
        scores_array.append(score)
        condition = condition + 1
    
    return scores_array


def max(scores_array):
    maximum = scores_array[0]
    for index in range(1, len(scores_array)):
        if maximum < scores_array[index]: 
            maximum = scores_array[index] 
    return maximum 


def min(scores_array): 
    minimum = scores_array[0] 
    for index in range(1, len(scores_array)): 
        if minimum > scores_array[index]:
            minimum = scores_array[index]
    return minimum


def calculate_average(scores_array):    
    average = sum(scores_array) / len(scores_array)
    return average


def display_max(maximum):
     print("The highest score you've entered is: " + str(maximum) + ".")
     
     
def display_min(minimum):
    print("The lowest score you've entered is: " + str(minimum) + ".")


def display_average(average):
    print("An average of entered scores is: " + str(average) + ".")


def main():
    scores_number = get_scores_number()
    scores_array = get_array(scores_number)

    maximum = max(scores_array)
    minimum = min(scores_array)
    
    average = calculate_average(scores_array)
    
    display_max(maximum)
    display_min(minimum)
    
    display_average(average)

      
main()