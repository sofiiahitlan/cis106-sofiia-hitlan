#This program gets a line of text from the user and displays it backwards.

def get_text():
    print("Please enter a line of text you would like to turn backwards: ")
    text = str(input())
    
    return text


def get_trim(text):
    trim = text.strip(' ')
    
    return trim


def get_reverse(trim):
    reverse = trim[::-1]
    
    return reverse


def display_result(reverse):
    print("Your converted text is: " + reverse)
    

def main():
    text = get_text()
    trim = get_trim(text)
    reverse = get_reverse(trim)
    display_result(reverse)
 
 
main()