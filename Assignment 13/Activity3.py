#This programs asks user to make a a grocery list. Items have to be separated by comma. Program then display items on seperate lines.

def get_text():
    print("Please enter items you need to get in grocery store on your next trip separating them by comma: ")
    text = str(input())
    
    return text


def get_new_line(text):
    new_line = text.replace(' ', '\n')
    
    return new_line


def replace_comma(new_line):
    new_line = new_line.replace(',', '')
    
    return new_line


def display_result(new_line):
    print("Your items are now printed on seperate lines: " + '\n' + new_line)
    

def main():
    text = get_text()
    new_line = get_new_line(text)
    new_line = replace_comma(new_line)
    display_result(new_line)
 
 
main()