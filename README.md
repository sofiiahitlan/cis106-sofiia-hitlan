Sofiia Hitlan

# Assignment 1

I'm taking CIS106 because it's required course in Web Development AAS program. I'm not aware of the difference between WEB, CIS and CSC classes and hope to find it out this semester. I've selected Python as a programming language for this course because I heard that it's the most wanted language that developers would like to learn. There are a lot of occupation in the IT field and practicing Python will take me one step ahead in figuring out who I want to be. Applying Python to server-side web development and database management will help me build a more professional portfolio.

# Assignment 2

This week I have learned that planning has to be done before the coding can be started. Planning is a long process which may not be as exciting as coding is, but it can save programmer a lot of time later on.  Pseudocode and flowcharts are tools used for program planning and design. While the flowchart is a visual diagram, pseudocode is a set of written instructions. I’ll practice using both methods to find out which one I prefer and which one I’ll be using in a professional environment in the future while working on a project. 

Also, I learned that Version Control is used to save and store each revision of the program code in case it ever needs to be retrieved. It adds extra security and ensures that work won’t have to be started over. VC is helpful because it allows the programmer to experiment with the program.  If results are not satisfactory, VC allows to go back to the previous version.  VC is very helpful in a team environment because it allows multiple people to access the code from their devices.

When it goes about IDE, I learned that both cloud IDEs and desktop IDEs are available. Personal preference will help to determine which IDE to use. Also, some IDEs are used for specific programming languages only. Other ones can work with multiple languages.  IDE's advantage over text editor is that it includes also a debugger, interpreter and compiler. This fact will help me to better organize time at work because using one application instead of using multiple ones can prevent me from making mistakes (multitasking is not a better choice in this case) and save me some time. I’ve tested Repl.it and Thonny. Also, I’m considering using VSC later on. I’ve used it when I worked with HTML and CSS, so hopefully, this experience will be helpful.

# Assignment 3

This week I learned a difference between constants and variables. I discovered new to me PascalCase and snake_case casing standards and found out in what languages they are industry standards. I learned more about common data types and practiced outputting whole numbers as string elements in Python. This made me think that Python is a programming language with a lot of opportunities that make the coding process easy. I found myself overthinking and trying to access outside sources to find a solution to my problem, but end up using a basic command to get a desirable outcome. I’ll use this experience in my future (not only at work) to remind myself that answer is right there in front of me and I just need to look from another side to find a solution.

# Assignment 4

Also, I learned that just like not accurate and not logical code, extra comments can make program maintenance much harder. I'm sure being neat will be appreciated in the team environment and will help to avoid arguments as well. I'll try not to overload my code in the future to avoid problems.

# Assignment 5
This week I learned that functions in programming are used to prevent a repetition of code. It improves overall readability because the same chunks of code don't have to be inserted multiple times. This makes maintenance easier as well. Common functions have already been written by programmers and can be used to save time. I'll be taking advantage of such functions as soon as I'll be proficient enough because I think that using someone else's work without understanding the code won't have positive results. It won't help me become a better programmer.

# Assignment 6
This week I discovered that functions in Python require different formatting.   I've used pylintonline.com to autoformat my code and will keep using it to fix numerous mistakes I can't catch because of the lack of experience and because they are not as obvious after looking at the code for a few hours. 

# Assignment 7
This week I learned that if-else statements can help to develop a more complex program and can provide more opportunities to interact with the user. I found out that basic relational operators (>, <, <>, etc.) are used in programming and that “=” might not mean the same as “==”. 
I discovered a concept of how to execute same lines of code using loops, but couldn’t successfully implement it into my code. I’ll keep practicing to make it happen so that my programs become more professional. 

# Assignment 8
I learned that While Loops will execute as many times as needed while condition is true. They are good to use when it's not clear how many values the user will want to enter. 

# Assignment 9
This week I learned that do loop doesn't exist in Python, but it might be emulated instead. To avoid infinite loops, there should be a condition. If it's true, the loop will continue to gather info. If it becomes false, the loop will break. Difference between while and simulated do loop is that in do loop statements are executed before condition is checked. It's important to know when deciding which loop to use when writing code.

# Assignment 10
I learned that for loop is helpful and is widely used when the number of times it will be executed is known. It's used for counting and increment option makes it even easier to use. Condition is tested before entering the loop which means the loop might not execute at all (as compared to the while do loop that must execute at least once before the condition is checked). It's handy to know the difference between different kinds of loops so that the right one is used for the project. 

# Assignment 11
I've learned that array is a set of elements that come in sequence and have one identifier name. Arrays can be searched for a certain element. They can be sorted in alphabetical and numerical order. Such order can be descending or ascending which will come in handy for databases administrators (I think).
In Python, arrays start from 0 which may be confusing at first. Loops are used to build an array. Python built-in functions make it easier to find the maximum or minimum value of an array, to calculate its sum or average. It adds some flexibility and helps to keep code neat.

# Assignment 12
I learned the difference between dynamic and static arrays. The first one can change its size and accept more arguments if required. Not-needed elements can be removed from the array.  While elements of the static array can be updated, its size can't be changed.  Loops are used to search for an element inside an array. Loop checks each array element and stops when the value is found or when the array ends. Maximum and minimum values can be found using linear search. 

# Assignment 13
This week I learned that functions can be used to modify and manipulate strings. Strings can be searched for an element,  concatenated, broken down to create substrings. Strings' elements can be replaced by other ones and case converted.

# Assignment 14
This week I learned that it's possible to use a computer file's data to create a program. Such files can be opened, viewed, changed and closed using programming language. A programmer has to define the file's path in order to use it for the project. Inputting large amounts of data from the keyboard takes a lot of time which can be more productively used for coding.  Files are time savers in this case.

# Final Project
This semester I learned that programming doesn't start from coding. Programming starts from throughout planning because it is logic-based science. The project has to be split into smaller parts. Each part has to be tested and work before the next one can be coded. While warning errors are not always easy to understand, debuggers do a good job showing what part of the code doesn't work correctly.  I'll keep using them to make sure my code works as intended. Even though this semester was short, learned concepts allow me to code basic programs and make such programs more structural by including error handling. I believe that using error handling reflects the programmer's professionalism and therefore I'll try to use it for my programs so that I can create my portfolio one day and be happy with its contents.