def getScore():
    print("Please enter a score")
    score = int(input())
    
    return score

def getScoresNumber():
    print("How many scores would you like to enter?")
    scoresNumber = int(input())
    
    return scoresNumber

# Main
scoresNumber = getScoresNumber()
condition = 0
total = 0
while condition < scoresNumber:
    score = getScore()
    total = total + score
    condition = condition + 1
print("Your scores average is " + str(float(total) / scoresNumber))
