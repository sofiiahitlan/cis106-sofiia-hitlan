def getExpressions():
    print("Enter number of expressions")
    expressionsNumber = int(input())
    
    return expressionsNumber

def getNumber():
    print("Enter numeric value you would like to see a list of multiplication expressions for")
    number = int(input())
    
    return number

# Main
# This program generates list of multiplication expressions for a value entered by user. User enters number of expressions.
condition = 0
number = getNumber()
expressionsNumber = getExpressions()
while condition < expressionsNumber:
    condition = condition + 1
    print(str(number) + " * " + str(condition) + " = " + str(number * condition))