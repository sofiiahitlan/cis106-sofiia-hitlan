#This program converts miles to kilometers, meters, and centimeters.

MILE_IN_KM = 1.609344
MILE_IN_M = 1609.344
MILE_IN_CM = 160934.4

print("Please provide mile value you would like to convert to kilometers, meters, and centimeters")
miles = int(input())

miles_to_km = miles * MILE_IN_KM
miles_to_m = miles * MILE_IN_M
miles_to_cm = miles * MILE_IN_CM

print(str(miles) + " miles are equal to " + str(miles_to_km) + " kilometers")
print(str(miles) + " miles are equal to " + str(miles_to_m) + " meters")
print(str(miles) + " miles are equal to " + str(miles_to_cm) + " centimeters")
