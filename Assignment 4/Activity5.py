#This program calculates the area of a room to determine the amount of floor covering needed.

print("Enter a length of your room in feet")
length = float(input())

print("Enter a width of your room in feet")
width = float(input())

square_feet = length * width
square_yards = square_feet / 9

print("You will need " + str(square_yards) + " yards of floor covering for your room")
